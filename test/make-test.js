// For loading browser modules
global.requirejs = require('requirejs');

global.requirejs.config({
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        main:       '../main'
    }
});

var Mocha = require("mocha");

// create mocha instance and pass options
var mocha = new Mocha({ ui: 'bdd', reporter: 'spec'});

// load assertion frameworks
global.chai = require('chai');

// define window after loading modules
global.window = global;

// add files 
mocha.addFile('src/PixelGrid')
mocha.addFile('src/PixelGrid/queryGrid')
mocha.addFile('src/EmotiGrid')
mocha.run();
