var EmotiGrid = requirejs('main/src/EmotiGrid')

describe('EmotiGrid', function(){
    it('Initialise grid returns grid', function(){
        var grid = { rowCount: 2, colCount: 2 }
           ,element 
           ,colours // simple test doubles
           ,emotiGrid = new EmotiGrid(element, grid, colours)

        var map = emotiGrid.initGrid('(bandit)')

        chai.expect(map).to.deep.equal(['(bandit)','(bandit)','(bandit)','(bandit)'])
    })

    it('Draw Grid returns string', function(){
        var grid = { rowCount: 2, colCount: 2 }
           ,element = {}
           ,colours // simple test doubles
           ,emotiGrid = new EmotiGrid(element, grid, colours)

        var str = emotiGrid.drawGrid('(bandit)')

        chai.expect(str).to.match(/bandit/)
    })
})