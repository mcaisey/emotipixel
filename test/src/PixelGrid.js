var PixelGrid = requirejs('main/src/PixelGrid')


// Test double for canvas
var getCanvasCtx = function() {
    var fillRectCount = 0
       ,canvasCtx = function(){}
       ,fillRect = function(){ fillRectCount++ }
    canvasCtx.fillRect = fillRect
    canvasCtx.fillStyle = null
    canvasCtx.getRectCount = function(){ return fillRectCount }
    return canvasCtx
}

var grid = {
    width:    10,
    height:   10,
    rowCount: 9,
    colCount: 18,
    spacer:   2
}

var pixelBuilder = function(){}
pixelBuilder.buildPixel = function(){
    return {}
}

describe('PixelGrid', function(){
    it('Draw grid calls fill rectangle n times on canvas context', function(){
        var context = getCanvasCtx()
        var pixelGrid = new PixelGrid(context, grid, pixelBuilder)
        pixelGrid.drawGrid()
        chai.expect(context.getRectCount()).to.be.equal(162) // 9 * 18
    })
})