var queryGrid = requirejs('main/src/PixelGrid/queryGrid')



var grid = {
    width:    10,
    height:   10,
    rowCount: 9,
    colCount: 18,
    spacer:   2
}

describe('Query grid struct', function(){


    it('Mouse over pixel returns point', function(){
        var point = queryGrid.mouseOverPixel(1, 1, grid)
        chai.expect(point).to.be.deep.equal({ row: 0, col: 0 }) // first row, first column
    })
})