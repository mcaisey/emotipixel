define(function(){

    var EmotiGrid = function(element, grid, colours){
        this.element = element
        this.grid    = grid
        this.colours = colours
        this.map     = []
    }

    EmotiGrid.prototype.drawGrid = function(colour) {
        if (colour !== undefined) {
            this.initGrid(colour)
        }
        var str = ''
        for (var i = 0; i < this.map.length; i++) {
            if (i % this.grid.colCount === 0) {
                str += "\n"
            }
            str += this.map[i]
        }
        str = str.replace(/          /g, '           ') // replace 10 with 11 // marginal editing
        this.element.innerText = str

        return str
    }

    /*
     * Initialise the map with one colour
     */
    EmotiGrid.prototype.initGrid = function(colour) {
        var pixels = this.grid.rowCount * this.grid.colCount
        for(var i = 0; i < pixels; i++) {
            this.map[i] = colour
        }
        return this.map
    }

    EmotiGrid.prototype.drawPixel = function(point, colour) {
        var mapId = point.col + (point.row * this.grid.colCount)
        this.map[mapId] = colour
        this.drawGrid()
    }

    return EmotiGrid
})