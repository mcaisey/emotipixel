// Bootstrap

// Elements for context
var canvas = document.getElementById("canvas")
var element = document.getElementById('emotipixel')

// Grid struct
var grid = {
    width: 10,
    height: 10,
    rowCount: 9,
    colCount: 18,
    spacer: 2
}

// Start application
require(['PixelGrid', 
    'PixelGrid/pixelBuilder', 
    'PixelGrid/queryGrid', 
    'EmotiGrid', 
    'Colours'], function(PixelGrid, pixleBuilder, queryGrid, EmotiGrid, Colours) {

    if (canvas.getContext) {
        var canvasContext = canvas.getContext("2d")
    }
    var colours = new Colours()
    var pixleGrid = new PixelGrid(canvasContext, grid, pixleBuilder)
    var emotiGrid = new EmotiGrid(element, grid, colours)

    // Initialise view
    var initGrid = (function(){
        pixleGrid.drawGrid(colours.idToPixelColour('white'))
        emotiGrid.drawGrid(colours.idToEmotiColour('white'))
        return this
    })()

    // Click events
    var clearBtn = document.getElementById('cleargrid')
    clearBtn.addEventListener('click', function(){
        initGrid()
    })

    // Mouse events
    var mousedown = false
    canvas.addEventListener("mousedown", function(e){
        drawPixel(e)
        mousedown = true
    }, false)

    window.addEventListener('mouseup', function(e){
        mousedown = false
    }, false)

    canvas.addEventListener('mousemove', function(e){
        if (mousedown === true) {
            drawPixel(e)
        }
    }, false)

    // Utility (controller) methods
    var selectedColour = function(){
        var colours = document.getElementsByName("colours")
        for (var i = colours.length - 1; i >= 0; i--) {
            if (colours[i].checked == true) {
                return colours[i].value
            }
        }
    }

    var drawPixel = function(e){
        var elementRect = canvas.getBoundingClientRect()
           ,x = e.pageX - elementRect.left
           ,y = e.pageY - elementRect.top
        var point = queryGrid.mouseOverPixel(x, y, grid)    
        pixleGrid.drawPixel(point, colours.idToPixelColour(selectedColour()))
        emotiGrid.drawPixel(point, colours.idToEmotiColour(selectedColour()))
    }
})