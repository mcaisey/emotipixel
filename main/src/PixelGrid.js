define(function(){

    var self;

    /*
     * Constructor
     */
    PixelGrid = function(canvasCtx, grid, pixelBuilder){
        self = this
        this.grid = grid
        this.canvasContext = canvasCtx
        this.pixelBuilder = pixelBuilder
    }

    /*
     * draw grid
     */
    PixelGrid.prototype.drawGrid = function(colour) {
        var x = 0
           ,y = 0
           ,col = 0
           ,row = 0
           ,pixels = this.grid.rowCount * this.grid.colCount

        for (var i = 0; i < pixels; i++) {

            col = i % this.grid.colCount
            row = Math.floor(i / this.grid.colCount)

            this.drawPixel({'row': row, 'col': col}, colour)
        }
    }

    /*
     * draw pixel
     */
    PixelGrid.prototype.drawPixel = function(point, colour) {
        var pixel = this.pixelBuilder.buildPixel(this.grid, point, colour)
        this.canvasContext.fillStyle = pixel.colour
        this.canvasContext.fillRect(pixel.x, pixel.y, pixel.width, pixel.height)
    }

    return PixelGrid
})