define(function(){
    var PixelBuilder = function(){}

    /*
     * Build pixel
     * @arg grid struct
     * @arg point struct
     * @arg colour string
     * @return pixel struct
     */
     PixelBuilder.prototype.buildPixel = function(grid, point, colour) {
        var width = grid.width
           ,height = grid.height
           ,spacer = grid.spacer
           ,x = (width + spacer) * point.col
           ,y = (height + spacer) * point.row

        var pixel = {
            'colour': colour,
            'x':      x,
            'y':      y,
            'width':  width,
            'height': height
        }
        return pixel
     }

    return new PixelBuilder()
})