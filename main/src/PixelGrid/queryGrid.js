define(function(){
    var QueryGrid = function(){}

    /*
     * Determines which pixel the mouse is over
     * @return point struct
     */
    QueryGrid.prototype.mouseOverPixel = function(x, y, grid) {
        var width = grid.width + grid.spacer
           ,height = grid.height + grid.spacer
           ,col = Math.ceil(x / width) - 1 
           ,row = Math.ceil(y / height) - 1

        row = this.rowInBounds(row, grid)
        col = this.colInBounds(col, grid)
        
        return {'row': row, 'col': col}
    }

    /*
     * Keeps the row in boundary
     */
    QueryGrid.prototype.rowInBounds = function(row, grid) {
        if (row >= grid.rowCount) {
            row = grid.rowCount - 1
        } 
        
        if (row < 0) {
            row = 0
        } 

        return row
    }

    /*
     * Keeps the column in boundaries
     */
    QueryGrid.prototype.colInBounds = function(col, grid) {
        if (col >= grid.colCount) {
            col = grid.colCount - 1
        } 

        if (col < 0) {
            col = 0
        }

        return col
    }

    return new QueryGrid()
})