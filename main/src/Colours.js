define(function(){

    Colours = function(){
        this.pixelColours = {
            white:  'rgb(244,244,244)', 
            green:  'rgb(0,200,0)', 
            black:  'rgb(0,0,0)', 
            red:    'rgb(200,0,0)', 
            yellow: 'rgb(255,255,0)',
            beige:  'rgb(255,255,224)'
        }

        this.emotiColours = {
            white:  '     ', 
            green:  '(envy)', 
            black:  '(bandit)', 
            red:    '(heart)', 
            yellow: '(rofl)',
            beige:  '(coffee)'
        }

        this.intColours = {
            white:  0, 
            green:  1, 
            black:  2, 
            red:    3, 
            yellow: 4,
            beige:  5
        }
    }

    Colours.prototype.idToPixelColour = function(id) {
        return this.pixelColours[id]
    }

    Colours.prototype.idToEmotiColour = function(id) {
        return this.emotiColours[id]
    }

    return Colours
})