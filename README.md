-- Pixel Skype  18/07/14 --

Use emoticons to draw pixel drawings

-- Install --

Install for main
- make install
- bower install

Install for test
- make install

-- To do --
Make into bookmarklet
Copy in from skype
 - on change/paste parse
 - if cannot parse cancel
 - redraw grid from previous
Use spaces instead for output
Change grid size & zoom
To clipboard event
shift image
grow grid (canvas)
Google apps it

-- Resources -- 
https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Canvas_tutorial
